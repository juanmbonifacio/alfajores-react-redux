import textos from '../store/idiomaTextos';

const store = {
	alfajor: {
		data: [],
		response: {}
	},
	response: {},
	idioma: textos.es
};
export default store;
